package com.arzhelig.meo.tool;

public interface ConstantesMEO {
	
	String 	PROG_NAME						= "MEO";
	String 	PROG_VERSION					= "V0.3";
	
	
	Integer P_MAIN_WIDTH					=	600;
	Integer P_MAIN_HEIGHT					=	300;	
	
	String 	P_FICHEAUTEUR_NAME				=	"Fiche auteur";
	Integer P_FICHEAUTEUR_WIDTH				=	600;
	Integer P_FICHEAUTEUR_HEIGHT			=	400;
	
	String 	B_QUITTER_TEXTE					= "QUITTER";	
	Integer B_QUITTER_WIDTH					=	100;
	Integer B_QUITTER_HEIGHT				=	20;
	
	
	String 	B_VALIDER_TEXTE					= "VALIDER";	
	Integer B_VALIDER_WIDTH					=	100;
	Integer B_VALIDER_HEIGHT				=	20;
	
	String 	B_AJOUTER_TEXTE					= "AJOUTER";	
	Integer B_AJOUTER_WIDTH					=	100;
	Integer B_AJOUTER_HEIGHT				=	20;
	
	String 	B_RECHERCHER_TEXTE				= "RECHERCHER";	
	Integer B_RECHERCHER_WIDTH				=	150;
	Integer B_RECHERCHER_HEIGHT				=	20;
	
	Integer SPINNER_STANDARD_WIDTH			=	80;
	Integer SPINNER_STANDARD_HEIGHT			=	20;
	

	Integer B_STANDARD_WIDTH				=	100;
	Integer B_STANDARD_HEIGHT				=	20;

	



	
}
