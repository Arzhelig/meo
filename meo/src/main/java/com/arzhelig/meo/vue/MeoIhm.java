package com.arzhelig.meo.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.RoundingMode;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;

import com.arzhelig.meo.dao.Auteur;
import com.arzhelig.meo.service.AuteurService;
import com.arzhelig.meo.tool.ConstantesMEO;

//	https://docs.oracle.com/javase/tutorial/uiswing/
//	https://docs.oracle.com/javase/tutorial/uiswing/components/table.html


public class MeoIhm implements ActionListener {
	
	final static Logger logger = Logger.getLogger(MeoIhm.class);
	
	// Elements generiques
	private JButton 	bouton_QUIT;	
	private JButton 	bouton_NOP;
	
	// Elements fenetre ListeAuteur
	private JFrame		frame_Main;
	private JPanel 		panel_ListeAuteur;
	private JButton 	b_ListeAuteur_AjouterAuteur;
	private JButton 	b_ListeAuteur_EditerAuteur;
	private JButton 	b_ListeAuteur_Quitter;
	
	// Elements fenetre AjoutAuteur
	private JFrame		frame_AjoutAuteur;
	private JPanel 		panel_AjoutAuteur;
	private JButton 	b_AjoutAuteur_Ajouter;
	private JButton 	b_AjoutAuteur_Quitter;
	private JTextField 	tf_AjoutAuteur_Nom;
	private JTextField 	tf_AjoutAuteur_Prenom;

	// Elements fenetre FicheAuteur
	private JFrame		frame_FicheAuteur;
	private JPanel 		panel_FicheAuteur;
	private JButton 	b_FicheAuteur_Valider;
	private JButton 	b_FicheAuteur_RemplacerId;
	private JButton 	b_FicheAuteur_Quitter;
	private JSpinner 	sp_FicheAuteur_RechercheId;
	private JSpinner 	sp_FicheAuteur_Id;
	private JTextField 	tf_FicheAuteur_Nom;
	private JTextField 	tf_FicheAuteur_Prenom;

	
	// Divers	
	AuteurService auteurService = new AuteurService();	
	ComposantMeo composantMeo = new ComposantMeo();
	
	

	public MeoIhm(){
		logger.debug("MeoIhm : lancement" );
		frame_Main = createMainFrame();
	}

	
	private JFrame createMainFrame(){		
	    frame_Main = new JFrame(ConstantesMEO.PROG_NAME);	    
	    frame_Main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	    frame_Main.setResizable(false); 	    
	    frame_Main.setSize(ConstantesMEO.P_MAIN_WIDTH, ConstantesMEO.P_MAIN_HEIGHT);
	    
   	    panel_ListeAuteur = createPanelListeAuteur();
   	    frame_Main.setContentPane(panel_ListeAuteur);
   	    
   	    frame_Main.setVisible(true);
   	    
   	    logger.debug("MeoIhm : creation mainFrame" );	    
		return frame_Main;
	}
	
	/*
	 * Panneau principal 
	 * 
	 */
	
	private JPanel createPanelListeAuteur(){
	    JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel panelCenter=new JPanel();
		JPanel panelSouth=new JPanel();

		panelCenter.setPreferredSize(new Dimension(200, 200));	    
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));		
		
		//b_ListeAuteur_Table = new JTable(getTableListeAuteurValue(), getTableListeAuteurColumn());		
        JTable b_ListeAuteur_Table = new JTable(new ListeAuteurTableModel(composantMeo.getTableListeAuteurValue(auteurService), composantMeo.getTableListeAuteurColumn()));

        //b_ListeAuteur_Table.getColumn("Editer").setCellRenderer(new ButtonRenderer());
        //b_ListeAuteur_Table.getColumn("Editer").setCellEditor(new ButtonEditor(new JCheckBox()));
        
		b_ListeAuteur_Table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		b_ListeAuteur_Table.setFillsViewportHeight(true);
        
        JScrollPane scrollPane = new JScrollPane(b_ListeAuteur_Table);
        panelCenter.add(scrollPane);
		

        b_ListeAuteur_AjouterAuteur = new JButton("Ajouter Auteur");	
        b_ListeAuteur_AjouterAuteur.addActionListener(this);
		panelSouth.add(b_ListeAuteur_AjouterAuteur);

		b_ListeAuteur_EditerAuteur = new JButton("Editer Auteur");	
		b_ListeAuteur_EditerAuteur.addActionListener(this);
		panelSouth.add(b_ListeAuteur_EditerAuteur);
		
		b_ListeAuteur_Quitter = new JButton(ConstantesMEO.B_QUITTER_TEXTE);		
		b_ListeAuteur_Quitter.addActionListener(this);
		panelSouth.add(b_ListeAuteur_Quitter);
	    
		panel.add("Center",panelCenter);
		panel.add("South",panelSouth);
	    	    
	    return panel;	
	}
		
	
	

	
	
	
	
	
	
	
	
	private JFrame createAjoutAuteurFrame(){
		
	    frame_AjoutAuteur = new JFrame("Ajout auteur");
	    frame_AjoutAuteur.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    frame_AjoutAuteur.setResizable(false);
	    frame_AjoutAuteur.setSize(500, 150);

	    panel_AjoutAuteur = createPanelAjoutAuteur();
	    frame_AjoutAuteur.setContentPane(panel_AjoutAuteur);
	    
	    frame_AjoutAuteur.setVisible(true);	    
	    logger.debug("MeoIhm : creation ajoutAuteurFrame" );
	    return frame_AjoutAuteur;
	}
	
	private JPanel createPanelAjoutAuteur(){
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());		
		
		tf_AjoutAuteur_Nom = new JTextField();
		tf_AjoutAuteur_Nom.setColumns(40);
		tf_AjoutAuteur_Nom.setText("Nom auteur"); 
		panel.add(tf_AjoutAuteur_Nom);

		tf_AjoutAuteur_Prenom= new JTextField();
		tf_AjoutAuteur_Prenom.setColumns(40);
		tf_AjoutAuteur_Prenom.setText("Prénom auteur"); 
		panel.add(tf_AjoutAuteur_Prenom);
		
		b_AjoutAuteur_Ajouter = new JButton(ConstantesMEO.B_AJOUTER_TEXTE);		
		b_AjoutAuteur_Ajouter.addActionListener(this);
		panel.add(b_AjoutAuteur_Ajouter);
		
		b_AjoutAuteur_Quitter = new JButton(ConstantesMEO.B_QUITTER_TEXTE);		
		b_AjoutAuteur_Quitter.addActionListener(this);
		panel.add(b_AjoutAuteur_Quitter);
		
		return panel;
	}
	
	
	
	
	
	
	
	
	private JFrame createFicheAuteurFrame(){		
		frame_FicheAuteur = new JFrame(ConstantesMEO.P_FICHEAUTEUR_NAME);	    
		frame_FicheAuteur.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame_FicheAuteur.setResizable(false); 	    
		frame_FicheAuteur.setSize(ConstantesMEO.P_FICHEAUTEUR_WIDTH, ConstantesMEO.P_FICHEAUTEUR_HEIGHT);
	    
   	    panel_FicheAuteur = createPanelFicheAuteur();
   	    frame_FicheAuteur.setContentPane(panel_FicheAuteur);
   	    
   	 	frame_FicheAuteur.setVisible(true);	    
	    logger.debug("MeoIhm : creation ficheAuteurFrame" );
		return frame_FicheAuteur;
	}
	
	private JPanel createPanelFicheAuteur(){
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, ConstantesMEO.P_FICHEAUTEUR_WIDTH, ConstantesMEO.P_FICHEAUTEUR_HEIGHT);
		panel.setLayout(null);
		Integer offsetX = 10;
		Integer offsetY = 10;
		Integer pasY = 30;
		//	Partie haute : INDEX + RECHERCHER + separator
		
		sp_FicheAuteur_RechercheId = new JSpinner(new SpinnerNumberModel(1, 0, 100000, 1));				
		sp_FicheAuteur_RechercheId.setBounds(offsetX, offsetY, ConstantesMEO.SPINNER_STANDARD_WIDTH, ConstantesMEO.SPINNER_STANDARD_HEIGHT);
		panel.add(sp_FicheAuteur_RechercheId);
		
		b_FicheAuteur_RemplacerId = new JButton(ConstantesMEO.B_RECHERCHER_TEXTE);
		b_FicheAuteur_RemplacerId.setBounds(100, offsetY,ConstantesMEO.B_RECHERCHER_WIDTH,ConstantesMEO.B_RECHERCHER_HEIGHT);
		b_FicheAuteur_RemplacerId.addActionListener(this);
		panel.add(b_FicheAuteur_RemplacerId);		
		
		offsetY = offsetY + pasY;
		panel.add(composantMeo.getSeparator(0, offsetY, ConstantesMEO.P_FICHEAUTEUR_WIDTH, 10));
		

//		Partie haute : autres Composants auteurs + VALIDER		
		offsetY = 60;
		
		sp_FicheAuteur_Id = new JSpinner(new SpinnerNumberModel(100000, 0, 100000, 1));
		sp_FicheAuteur_Id.setEnabled(false);
		sp_FicheAuteur_Id.setBounds(offsetX, offsetY, ConstantesMEO.SPINNER_STANDARD_WIDTH, ConstantesMEO.SPINNER_STANDARD_HEIGHT);
		
		panel.add(sp_FicheAuteur_Id);

		offsetY = offsetY + 20;
		tf_FicheAuteur_Prenom = new JTextField();		
		tf_FicheAuteur_Prenom.setBounds(offsetX, offsetY, 200, 20);
		panel.add(tf_FicheAuteur_Prenom);

		tf_FicheAuteur_Nom = new JTextField();		
		tf_FicheAuteur_Nom.setBounds(tf_FicheAuteur_Prenom.getWidth()+tf_FicheAuteur_Prenom.getX(), offsetY, 200, 20);
		panel.add(tf_FicheAuteur_Nom);
		
		b_FicheAuteur_Valider = new JButton(ConstantesMEO.B_VALIDER_TEXTE);
		b_FicheAuteur_Valider.setBounds(offsetX, 
				200,
				ConstantesMEO.B_QUITTER_WIDTH,
				ConstantesMEO.B_QUITTER_HEIGHT);		
		b_FicheAuteur_Valider.addActionListener(this);
		panel.add(b_FicheAuteur_Valider);		
		
		
		
		
		//	Partie basse : separator + QUITTER				
		offsetY = 350;
		JSeparator jSeparatorB = new JSeparator();
		jSeparatorB.setBounds(	0,	offsetY, 
								ConstantesMEO.P_FICHEAUTEUR_WIDTH, 1);
		panel.add(jSeparatorB);		
				
		offsetY = offsetY + 5;
		b_FicheAuteur_Quitter = new JButton(ConstantesMEO.B_QUITTER_TEXTE);		
		b_FicheAuteur_Quitter.setBounds(offsetX, offsetY, 
										ConstantesMEO.B_QUITTER_WIDTH,ConstantesMEO.B_QUITTER_HEIGHT);
		b_FicheAuteur_Quitter.addActionListener(this);
		panel.add(b_FicheAuteur_Quitter);		
							
		
		// Maj données		
		majFichePanel(1);	
		return panel;
	}
	
	
	private void majFichePanel(Integer id){		
		logger.debug("MeoIhm : majFichePanel" );
		
		Auteur auteur = auteurService.getAuteurById(id);
		if (auteur != null){
			sp_FicheAuteur_Id.setValue(auteur.getId());		
			tf_FicheAuteur_Nom.setText(auteur.getNom()); 
			tf_FicheAuteur_Prenom.setText(auteur.getPrenom());			
		}
		else {
			JOptionPane.showMessageDialog(null,"Aucun auteur trouvé !!!");
			logger.debug("MeoIhm : majFichePanel : Aucun auteur trouvé !!!" );
		}		
	}

	

	
	
	
	/**
	 * Gestion des evenements
	 */
	
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();		

		if(source == bouton_NOP)
		{				
			logger.debug("MeoIhm : actionPerformed : NOP" );
			JOptionPane.showMessageDialog(null, "bouton_NOP");
		}

		if(source == b_ListeAuteur_AjouterAuteur)
		{
			frame_AjoutAuteur = createAjoutAuteurFrame();
			frame_Main.dispose();			
			logger.debug("MeoIhm : actionPerformed : b_ListeAuteur_AjouterAuteur" );
		}
		
		if(source == b_AjoutAuteur_Ajouter)
		{	
			String textPrenom = tf_AjoutAuteur_Prenom.getText();
			String textNom = tf_AjoutAuteur_Nom.getText();
			auteurService.addAuteur(0, textNom, textPrenom);
			logger.debug("MeoIhm : actionPerformed : b_AjoutAuteur_Ajouter" );			
		}
		
		if(source == b_ListeAuteur_EditerAuteur)
		{
			frame_FicheAuteur = createFicheAuteurFrame();
			frame_Main.dispose();	
			logger.debug("MeoIhm : actionPerformed : b_ListeAuteur_EditerAuteur" );			

		}
		
		if(source == b_FicheAuteur_RemplacerId){			
			Integer id = (Integer) sp_FicheAuteur_RechercheId.getValue();
			majFichePanel(id);
			logger.debug("MeoIhm : actionPerformed : b_FicheAuteur_RemplacerId" );			
		}

			
		
		
		
		if(source == b_FicheAuteur_Quitter){
			frame_Main = createMainFrame();
			frame_FicheAuteur.dispose();		    
			logger.debug("MeoIhm : actionPerformed : b_FicheAuteur_Quitter" );
		}
		if(source == b_AjoutAuteur_Quitter){
			frame_Main = createMainFrame();
			frame_AjoutAuteur.dispose();		    
		    logger.debug("MeoIhm : actionPerformed : b_AjoutAuteur_Quitter" );
		}
		

		if(source == b_ListeAuteur_Quitter)
		{	
		    logger.debug("MeoIhm : actionPerformed : b_ListeAuteur_Quitter" );
			System.exit(0);
		}	
	}

}
