package com.arzhelig.meo.dao;

public class Auteur {

	private Integer id;
	private String nom;
	private String prenom;
	private Boolean vivant;
	
	public Auteur(Integer id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		if (prenom.length() > 5){
			vivant = true;	
		} else {
			vivant = false;
		}
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Boolean getVivant() {
		return vivant;
	}

	public void setVivant(Boolean vivant) {
		this.vivant = vivant;
	}

	@Override
	public String toString() {
		return "Auteur [id = <" + id + ">, nom=<" + nom + ">, prenom=<" + prenom + ">]";
	}
	
	
	
	
	
}
