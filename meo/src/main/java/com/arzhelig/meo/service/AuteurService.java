package com.arzhelig.meo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.arzhelig.meo.dao.Auteur;
import com.arzhelig.meo.vue.MeoIhm;

public class AuteurService {
	List <Auteur> listeAuteur = new ArrayList<Auteur>(1000) ;
	final static Logger logger = Logger.getLogger(AuteurService.class);

	
	public AuteurService(){
		initAuteur();
	}
	
	public void initAuteur(){		
		logger.info("AuteurService : Initialisation des données");
		listeAuteur.add(new Auteur (1,"Verne","Jules"));
		listeAuteur.add(new Auteur (2,"Hugo","Victor"));
		listeAuteur.add(new Auteur (3,"Foenkinos","David"));
		listeAuteur.add(new Auteur (4,"Irving","John"));		
		listeAuteur.add(new Auteur (5,"Kinsella","Sophie"));
		listeAuteur.add(new Auteur (6,"Ludlum","Robert"));
		listeAuteur.add(new Auteur (7,"Oates Joyce","Carol"));
		listeAuteur.add(new Auteur (8,"Roberts","Nora"));
		listeAuteur.add(new Auteur (9,"Winckler","Martin"));
		listeAuteur.add(new Auteur (10,"Pagnol","Marcel"));
	}
	
	private Integer getNewId(){
		Integer id = 0;
		for(Auteur auteur : listeAuteur){						
			id = (auteur.getId()> id ? auteur.getId(): id );
		}		
		return ++id;
	}	
	
	public Auteur getAuteurById(Integer id){
		for (Auteur auteur : listeAuteur){
			if (auteur.getId() == id ){
				logger.debug("AuteurService : getAuteurById :" + auteur.toString());
				return auteur;
			}
		}
		logger.debug("getAuteurById : NULL" );
		return null;		
	}
	
	public void addAuteur(Auteur auteur){		
		logger.debug("AuteurService : addAuteur :" + auteur.toString());
		listeAuteur.add(auteur);		
	}

	public void addAuteur(Integer id, String nom, String prenom){
		Integer newId = ( id == 0 ? getNewId() : id);
				
		Auteur auteur = new Auteur(newId, nom, prenom);
		listeAuteur.add(auteur);	
		logger.debug("AuteurService : addAuteur :" + auteur.toString());		
	}
	
	public void removeAuteur(Auteur auteur){
		logger.debug("AuteurService : removeAuteur :" + auteur.toString());
		listeAuteur.remove(auteur);
	}
	
	public void removeAuteur(Integer id){		
		Auteur auteur = null;
		for (Auteur a: listeAuteur){
			if (a.getId() == id ){
				auteur = a;
			}
		}
		if (auteur != null){
			logger.debug("AuteurService : removeAuteurViaId :" + auteur.toString());
			removeAuteur(auteur);
		}
		else {
			logger.debug("AuteurService : removeAuteurViaId not find : <"  + id + ">");
		}			
	}
	
	public List<Auteur> getListAuteur(){		
		logger.debug("AuteurService : getListAuteur :" + listeAuteur.size());
		return listeAuteur;
	}	
}

