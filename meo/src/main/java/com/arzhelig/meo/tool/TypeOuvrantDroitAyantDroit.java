package com.arzhelig.meo.tool;

import java.util.HashMap;
import java.util.Map;

public enum TypeOuvrantDroitAyantDroit {
	INCONNU("Inconnu",1L)	, 
	AYANT_DROIT("Ayant droit",2L)	, 
	OUVREUR_DROIT("Ouvreur de droits",3L)
	;

	public static TypeOuvrantDroitAyantDroit getValue(long unId)		{
		TypeOuvrantDroitAyantDroit retour = null;

		if (unId == INCONNU.id){
			retour = INCONNU;
		} else if (unId == AYANT_DROIT.id) {
			retour = AYANT_DROIT;
		} else if (unId == OUVREUR_DROIT.id) {
			retour = OUVREUR_DROIT;
		}
		return retour;
	}


	private String libelle ="";
	private Long id;

	TypeOuvrantDroitAyantDroit(String aName, Long anId){
		libelle = aName;
		id = anId;
	}


	public String getLibelle(){
		 return(libelle);
	}

	public Long toLong(){
		 return(id);
	}


    public static TypeOuvrantDroitAyantDroit valueOf(Long id) {
        return longMap.get(id);
    }


	 private static Map<String, TypeOuvrantDroitAyantDroit> namesMap = new HashMap<String, TypeOuvrantDroitAyantDroit>(3);
	 private static Map<Long, TypeOuvrantDroitAyantDroit> longMap = new HashMap<Long, TypeOuvrantDroitAyantDroit>(3);
	 static {
		 namesMap.put("Inconnu", INCONNU);
		 longMap.put(1L, INCONNU);
		 namesMap.put("Ayant droit", AYANT_DROIT);
		 longMap.put(2L, AYANT_DROIT);
		 namesMap.put("Ouvreur de droits", OUVREUR_DROIT);
		 longMap.put(3L, OUVREUR_DROIT);
	}



	public static TypeOuvrantDroitAyantDroit forValue(String value) {
		return TypeOuvrantDroitAyantDroit.valueOf(value);
	}

	public String toValue() {
		return this.name();
	}

	 public static String donnerReferentiel () {
		 	return "TRANS_TYPEOUVRANTDROITAYANTDROIT = [\n" +
	                "\t{\"id\" : \"1\", \"libelle\" : \"Inconnu\"},\n" +
	                "\t{\"id\" : \"2\", \"libelle\" : \"Ayant droit\"},\n" +
	                "\t{\"id\" : \"3\", \"libelle\" : \"Ouvreur de droits\"}\n" +
	                "]";
		}

	}

