package com.arzhelig.meo.vue;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

public class ButtonRenderer extends JButton implements TableCellRenderer {

    /**
	 * 
	 */
	private static final long serialVersionUID = 198L;
	  

	public ButtonRenderer() {
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(UIManager.getColor("Button.background"));
        }
        setText((value == null) ? "" : value.toString());
        System.out.println("     ButtonRenderer : " + row);
        return this;
    }

}

class ButtonEditor extends DefaultCellEditor {

    /**
	 * 
	 */
	private static final long serialVersionUID = 197L;
	protected JButton button;
    private String label;
    private boolean isPushed;

	private ButtonListener bListener = new ButtonListener();

	
    public ButtonEditor(JCheckBox checkBox) {
        super(checkBox);
        button = new JButton();
        button.setOpaque(true);
    	System.out.println( "Renderer "  );
    	/*
    	try {	//	src/resources
    		Image img = ImageIO.read(getClass().getResource("/meo/src/ressources/crayon.png"));
    		button.setIcon(new ImageIcon(img));
    	} catch (Exception ex) {
			System.out.println( "Erreur bmp : " + ex.getMessage());
    	}
    	*/
        button.addActionListener(bListener);
/*    	    	
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
  */      
        
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
    	
		if (isSelected) {
            button.setForeground(table.getSelectionForeground());
            button.setBackground(table.getSelectionBackground());
            
        } else {
            button.setForeground(table.getForeground());
            button.setBackground(table.getBackground());
        }
        label = (value == null) ? "" : value.toString();
        button.setText(label);
        
        isPushed = true;

        System.out.println("     ButtonEditor : " + row);

        return button;
    }

    @Override
    public Object getCellEditorValue() {
        if (isPushed) {
            JOptionPane.showMessageDialog(button, label + ": Ouch!" );
        }
        isPushed = false;
        return label;
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
}
class ButtonListener implements ActionListener{


    private int column, row;

    private JTable table;

    private int nbre = 0;

    private JButton button;


    public void setColumn(int col){this.column = col;}

    public void setRow(int row){this.row = row;}

    public void setTable(JTable table){this.table = table;}


    public JButton getButton(){return this.button;}


    public void actionPerformed(ActionEvent event) {

    	System.out.println("coucou du bouton : " + ((JButton)event.getSource()).getText());
    	System.out.println("                   " + this.column + " - " + this.row);
    	System.out.println("                   " + ((JButton)event.getSource()).getName());

      //On affecte un nouveau libellé à une celulle de la ligne

      //((AbstractTableModel)table.getModel()).setValueAt("New Value " + (++nbre), this.row, (this.column -1));   

      //Permet de dire à notre tableau qu'une valeur a changé à l'emplacement déterminé par les valeurs passées en paramètres

      //((AbstractTableModel)table.getModel()).fireTableCellUpdated(this.row, this.column - 1);

      //this.button = ((JButton)event.getSource());

    }

  }