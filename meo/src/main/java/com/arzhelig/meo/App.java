package com.arzhelig.meo;

import com.arzhelig.meo.tool.ConstantesMEO;
import com.arzhelig.meo.vue.MeoIhm;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println( "___________________________________"  );
        System.out.println( ConstantesMEO.PROG_NAME + " " + ConstantesMEO.PROG_VERSION );
        System.out.println( "___________________________________"  );
        
        System.out.println( "Démarrage"  );
        MeoIhm meoIhm = new MeoIhm();        
    }
}
